package com.example.exemplojetpacknavigation


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_fragment_two.*


class FragmentTwo : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_fragment_two, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonTwo.setOnClickListener {
            arguments?.run {
                val name = getString("name")
                val args = Bundle().apply {
                    putString("name", name)
                    putString("age", "24")
                }
                Navigation.findNavController(requireActivity(),R.id.navHostFragment).navigate(R.id.action_fragmentTwo_to_fragmentThree, args)
            }
        }
    }
}
