package com.example.exemplojetpacknavigation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.internal.artificialFrame

class MainActivity : AppCompatActivity() {


    private val navController: NavController by lazy{
        Navigation.findNavController(this, R.id.navHostFragment)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        NavigationUI.setupActionBarWithNavController(this, navController)

    }


    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }



    //remove todos, menos o ultimo fragment
    override fun onBackPressed() {
        val id = navController.currentDestination?.id
        if (id != R.id.fragmentOne){
            navController.popBackStack()
        }
    }
}
