package com.example.exemplojetpacknavigation


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_fragment_one.*


class FragmentOne : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_fragment_one, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonOne.setOnClickListener {
            val args = Bundle().apply {
                putString("name","Rodrigo")
            }
            Navigation.findNavController(requireActivity(),R.id.navHostFragment).navigate(R.id.action_fragmentOne_to_fragmentTwo, args)
        }
    }
}
